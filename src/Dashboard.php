<?php

namespace Tetrapak07\Dashboard;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;

abstract class Dashboard extends Controller
{
      
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('dashboard')) {
            $this->dataReturn = false;
        } else {
            $this->view->setViewsDir($this->config->modules->dashboard->viewsDir);
            $this->view->setTemplateBefore($this->config->modules->dashboard->templateBefore);
        }   
    }
    
    public function onConstruct()
    {
       
    }

    public function indexAction()
    {
        if (!$this->dataReturn) {
            return false;
        }
    }
    
}
