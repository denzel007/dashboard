<?php

namespace App\Controllers;

use  App\Controllers\ControllerBase;

class DashboardController extends \Tetrapak07\Dashboard\Dashboard
{

    public function initialize()
    {
      # TO DO - check access to this superadmin module
        
      parent::initialize();
      
      $this->view->setViewsDir($this->config->modules->dashboard->viewsDir);
    }
    
    function onConstruct()
    {
        $base = new ControllerBase();
        
        $base->onConstruct();
        
    }    
    
    public function indexAction()
    {
      parent::indexAction();
    }
    
    
}